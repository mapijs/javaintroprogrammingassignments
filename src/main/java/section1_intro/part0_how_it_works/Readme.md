# An within-classroom assignment

We'll deal with (or start) this one in class so you know what to expect.

There are several methods shown in class `StartingJava`.

Study the Javadoc and the signature of each method and then implement the method body.
Verify the correctness of your solution by running the corresponding test method 
(to be found in `/src/test/java/week1_0/StartingJava.java`)

